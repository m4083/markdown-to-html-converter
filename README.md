# Markdown to HTML Converter
A simple commandline application that takes a text file in Markdown syntax, converts it to HTML, and prints it.

## Installation via Gitlab
These steps assume you have Git installed on your machine.
1. Open terminal.
2. Go to desired location in your directory.
3. Clone repository by running this command.
```bash
git clone https://gitlab.com/m4083/markdown-to-html-converter.git
```

## Run Application on Command-line
1. Open terminal.
2. Navigate to this gradle project.
3. Run the following command to build project.
```bash
./gradlew build
```
4. Run the following command to run project. Replace <file_path> with the file path to the text file you are converting.
```bash
./gradlew run --args=<file_path>
```
Example:
```bash
./gradlew run --args=src/test/resources/complexTestSample.txt
```

## Run Tests on Command-line
1. Open terminal.
2. Navigate to this gradle project.
3. Run the following command to run tests.
```bash
./gradlew test
```

## Current Conversions
| Markdown                               | HTML                                              |
| -------------------------------------- | ------------------------------------------------- |
| `# Heading 1`                          | `<h1>Heading 1</h1>`                              |
| `## Heading 2`                         | `<h2>Heading 2</h2>`                              |
| `### Heading 3`                        | `<h3>Heading 6</h3>`                              |
| `#### Heading 4`                       | `<h4>Heading 6</h4>`                              |
| `##### Heading 5`                      | `<h5>Heading 6</h5>`                              |
| `###### Heading 6`                     | `<h6>Heading 6</h6>`                              |
| `Unformatted text`                     | `<p>Unformatted text</p>`                         |
| `[Link text](https://www.example.com)` | `<a href="https://www.example.com">Link text</a>` |
| `Blank line`                           | `Ignored`                                         |

(Note: This ReadMe is in Markdown, but ironically will not convert properly to html because this application is super basic.)