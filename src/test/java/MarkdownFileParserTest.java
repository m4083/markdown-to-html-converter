import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MarkdownFileParserTest {
    @Test
    void Parses_simple_markdown_text_file_to_html() {
        String file = "src/test/resources/simpleTestSample.txt";

        MarkdownConverter markdownConverter = new MarkdownConverter();
        MarkdownFileParser markdownFileParser = new MarkdownFileParser(markdownConverter);
        String expectedResult = "<h1>Sample Document</h1>\n" +
                "\n" +
                "<p>Hello!</p>\n" +
                "\n" +
                "<p>This is sample markdown for the <a href=\"https://www.mailchimp.com\">Mailchimp</a> " +
                "homework assignment.</p>";
        String actualResult = markdownFileParser.parse(file);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Parses_complex_markdown_text_file_to_html() {
        String file = "src/test/resources/complexTestSample.txt";

        MarkdownConverter markdownConverter = new MarkdownConverter();
        MarkdownFileParser markdownFileParser = new MarkdownFileParser(markdownConverter);
        String expectedResult = "<h1>Header one</h1>\n" +
                "\n" +
                "<p>Hello there</p>\n" +
                "\n" +
                "<p>How are you?\n" +
                "What's going on?</p>\n" +
                "\n" +
                "<h2>Another Header</h2>\n" +
                "\n" +
                "<p>This is a paragraph <a href=\"http://google.com\">with an inline link</a>. Neat, eh?</p>\n" +
                "\n" +
                "<h2>This is a header <a href=\"http://yahoo.com\">with a link</a></h2>";
        String actualResult = markdownFileParser.parse(file);

        assertEquals(expectedResult, actualResult);
    }
}