import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarkdownConverterTest {
    @Test
    void Ignores_blank_line() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "";
        String actualResult = markdownConverter.convert("");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_unformatted_markdown_text_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<p>Unformatted text</p>";
        String actualResult = markdownConverter.convert("Unformatted text");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_markdown_link_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<a href=\"https://www.example.com\">Link text</a>";
        String actualResult = markdownConverter.convert("[Link text](https://www.example.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading1_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h1>Heading 1</h1>";
        String actualResult = markdownConverter.convert("# Heading 1");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading2_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h2>Heading 2</h2>";
        String actualResult = markdownConverter.convert("## Heading 2");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading3_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h3>Heading 3</h3>";
        String actualResult = markdownConverter.convert("### Heading 3");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading4_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h4>Heading 4</h4>";
        String actualResult = markdownConverter.convert("#### Heading 4");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading5_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h5>Heading 5</h5>";
        String actualResult = markdownConverter.convert("##### Heading 5");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading6_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h6>Heading 6</h6>";
        String actualResult = markdownConverter.convert("###### Heading 6");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_unformatted_text() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<p>This is sample markdown for the <a href=\"https://www.mailchimp.com\">Mailchimp</a> homework assignment.</p>";
        String actualResult = markdownConverter.convert("This is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading1() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h1>This is a header <a href=\"http://yahoo.com\">with a link</a></h1>";
        String actualResult = markdownConverter.convert("# This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading2() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h2>This is a header <a href=\"http://yahoo.com\">with a link</a></h2>";
        String actualResult = markdownConverter.convert("## This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading3() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h3>This is a header <a href=\"http://yahoo.com\">with a link</a></h3>";
        String actualResult = markdownConverter.convert("### This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading4() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h4>This is a header <a href=\"http://yahoo.com\">with a link</a></h4>";
        String actualResult = markdownConverter.convert("#### This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading5() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h5>This is a header <a href=\"http://yahoo.com\">with a link</a></h5>";
        String actualResult = markdownConverter.convert("##### This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_link_within_heading6() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h6>This is a header <a href=\"http://yahoo.com\">with a link</a></h6>";
        String actualResult = markdownConverter.convert("###### This is a header [with a link](http://yahoo.com)");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading1_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h1>Heading 1</h1>";
        String actualResult = markdownConverter.convert("#Heading 1");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading2_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h2>Heading 2</h2>";
        String actualResult = markdownConverter.convert("##Heading 2");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading3_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h3>Heading 3</h3>";
        String actualResult = markdownConverter.convert("###Heading 3");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading4_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h4>Heading 4</h4>";
        String actualResult = markdownConverter.convert("####Heading 4");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading5_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h5>Heading 5</h5>";
        String actualResult = markdownConverter.convert("#####Heading 5");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void Processes_heading6_with_no_space_after_hash_symbol_markdown_line_to_html() {
        MarkdownConverter markdownConverter = new MarkdownConverter();

        String expectedResult = "<h6>Heading 6</h6>";
        String actualResult = markdownConverter.convert("######Heading 6");

        assertEquals(expectedResult, actualResult);
    }
}