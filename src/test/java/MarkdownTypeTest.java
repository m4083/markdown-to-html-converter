import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarkdownTypeTest {
    @Test
    void Matches_Link_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.LINK;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("[Link text](https://www.example.com)");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading1_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING1;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("# Heading 1");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading2_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING2;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("## Heading 2");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading3_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING3;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("### Heading 3");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading4_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING4;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("#### Heading 4");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading5_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING5;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("##### Heading 5");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Heading6_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING6;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("###### Heading 6");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Unformatted_text_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.UNFORMATTED_TEXT;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("Unformatted text");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Matches_Blank_line_regex() {
        MarkdownType expectedMarkdownType = MarkdownType.BLANK_LINE;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Empty_brackets_and_parentheses_matches_unformatted_text() {
        MarkdownType expectedMarkdownType = MarkdownType.UNFORMATTED_TEXT;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("[]()");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Embedded_parentheses_in_brackets_matches_unformatted_text() {
        MarkdownType expectedMarkdownType = MarkdownType.UNFORMATTED_TEXT;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("[()]");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void One_Hash_symbol_with_no_space_after_should_still_match_heading1() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING1;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("#Heading 1");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Two_Hash_symbol_with_no_space_after_should_still_match_heading2() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING2;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("##Heading 2");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Three_Hash_symbol_with_no_space_after_should_still_match_heading3() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING3;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("###Heading 3");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Four_Hash_symbol_with_no_space_after_should_still_match_heading4() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING4;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("####Heading 4");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Five_Hash_symbol_with_no_space_after_should_still_match_heading5() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING5;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("#####Heading 5");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }

    @Test
    void Six_Hash_symbol_with_no_space_after_should_still_match_heading6() {
        MarkdownType expectedMarkdownType = MarkdownType.HEADING6;
        MarkdownType resultMarkdownType = MarkdownType.getLineType("######Heading 6");

        assertEquals(expectedMarkdownType, resultMarkdownType);
    }
}