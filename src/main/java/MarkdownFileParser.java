import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class MarkdownFileParser {

    private MarkdownConverter markdownConverter;

    public MarkdownFileParser(MarkdownConverter markdownConverter) {
        this.markdownConverter = markdownConverter;
    }

    public String parse(String fileName) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(markdownLine -> contentBuilder.append(markdownConverter.convert(markdownLine)).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return removeExtraneousPTags(contentBuilder.toString()).trim();
    }

    private String removeExtraneousPTags(String parsedString) {
        return parsedString.replaceAll("<p>([^\\n]+)</p>\n<p>([^\\n]+)</p>", "<p>$1\n$2</p>");
    }
}