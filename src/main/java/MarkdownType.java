public enum MarkdownType {

    LINK("\\[(.+)\\]\\(([^ ]+?)( \"(.+)\")?\\)"),
    HEADING1("(?m)^#(?!#)[ ]?(.*)"),
    HEADING2("(?m)^#{2}(?!#)[ ]?(.*)"),
    HEADING3("(?m)^#{3}(?!#)[ ]?(.*)"),
    HEADING4("(?m)^#{4}(?!#)[ ]?(.*)"),
    HEADING5("(?m)^#{5}(?!#)[ ]?(.*)"),
    HEADING6("(?m)^#{6}(?!#)[ ]?(.*)"),
    UNFORMATTED_TEXT("([^\\n]+)"),
    BLANK_LINE("\\s*");

    public final String regexPattern;

    MarkdownType(String regexPattern) {
        this.regexPattern = regexPattern;
    }

    static public MarkdownType getLineType(String markdownLine) {
        if (markdownLine.trim().isEmpty()) {
            return MarkdownType.BLANK_LINE;
        } else if (markdownLine.matches(LINK.regexPattern)) {
            return MarkdownType.LINK;
        } else if (markdownLine.matches(MarkdownType.HEADING1.regexPattern)) {
            return MarkdownType.HEADING1;
        } else if (markdownLine.matches(MarkdownType.HEADING2.regexPattern)) {
            return MarkdownType.HEADING2;
        } else if (markdownLine.matches(MarkdownType.HEADING3.regexPattern)) {
            return MarkdownType.HEADING3;
        } else if (markdownLine.matches(MarkdownType.HEADING4.regexPattern)) {
            return MarkdownType.HEADING4;
        } else if (markdownLine.matches(MarkdownType.HEADING5.regexPattern)) {
            return MarkdownType.HEADING5;
        } else if (markdownLine.matches(MarkdownType.HEADING6.regexPattern)) {
            return MarkdownType.HEADING6;
        } else {
            return MarkdownType.UNFORMATTED_TEXT;
        }
    }
}
