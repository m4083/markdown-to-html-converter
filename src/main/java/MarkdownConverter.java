public class MarkdownConverter {

    public String convert(String markdown) {
        MarkdownType markdownType = MarkdownType.getLineType(markdown);
        markdown = convertEmbeddedLinks(markdown);

        return switch (markdownType) {
            case BLANK_LINE -> "";
            case LINK -> markdown.replaceAll(MarkdownType.LINK.regexPattern, "<a href=\"$2\">$1</a>");
            case HEADING1 -> markdown.replaceAll(markdownType.HEADING1.regexPattern, "<h1>$1</h1>");
            case HEADING2 -> markdown.replaceAll(markdownType.HEADING2.regexPattern, "<h2>$1</h2>");
            case HEADING3 -> markdown.replaceAll(markdownType.HEADING3.regexPattern, "<h3>$1</h3>");
            case HEADING4 -> markdown.replaceAll(markdownType.HEADING4.regexPattern, "<h4>$1</h4>");
            case HEADING5 -> markdown.replaceAll(markdownType.HEADING5.regexPattern, "<h5>$1</h5>");
            case HEADING6 -> markdown.replaceAll(markdownType.HEADING6.regexPattern, "<h6>$1</h6>");
            default -> markdown.replaceAll(MarkdownType.UNFORMATTED_TEXT.regexPattern, "<p>$1</p>");
        };
    }

    private String convertEmbeddedLinks(String markdown) {
        return markdown.replaceAll(MarkdownType.LINK.regexPattern, "<a href=\"$2\">$1</a>");
    }
}
