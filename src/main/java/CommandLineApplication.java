public class CommandLineApplication {
    public static void main(String[] args) {
        String filePath = args[0];
        MarkdownConverter markdownConverter = new MarkdownConverter();
        MarkdownFileParser markdownFileParser = new MarkdownFileParser(markdownConverter);

        System.out.println(markdownFileParser.parse(filePath));
    }
}